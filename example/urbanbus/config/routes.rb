Rails.application.routes.draw do
  resources :users

  post "users/connect"
  post "users/answer"
  post "users/query"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "main/index"

  root "main#index"
end
