class CreateResponses < ActiveRecord::Migration[5.2]
  def change
    create_table :responses do |t|
      t.boolean :waiting
      t.boolean :accepted

      t.timestamps
    end
  end
end
