class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, :only => [:connect, :query, :answer]
  add_flash_types :error

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  def connect
    params.permit!

    if User.where(brid: params[:id]).empty?
      @user = User.new(
        brid: params[:id],
        name: params[:name]
      )

      notice = 'Welcome, ' + params[:name] + '!'

      if @user.save
        redirect_to @user, notice: notice
      end
    else
      @user = User.where(brid: params[:id]).first

      notice = 'Welcome back, ' + params[:name] + '!'

      redirect_to @user, notice: notice
    end
  end

  def query
    config.allow_concurrency=true
  
    params.require(:id)

    @user = User.find(params[:id])

    require "net/http"
    require "json"

    # @response = Response.create(waiting: true)

    uri = URI("http://localhost:3000/main/interact")
    http = Net::HTTP.new(uri.host, uri.port)
    req = Net::HTTP::Post.new(uri.path)
    req.set_form_data({
      brid: @user.brid.to_s,
      from_id: 2,
      key: "urbanbussecretkey",
      to_id: 1,
      interaction: "debit",
      interact: "http://localhost:3001/clients/debit",
      data: "3.0",
      request: 1, #@response.id,
      callback: "http://localhost:3002/users/answer"
    })
    res = http.request(req)

    # while Response.find(:id).waiting
    #   sleep(1)
    # end


    if res.body == "true"
      redirect_to @user, notice: "Transaction acepted."
    elsif res.body.to_s == "false"
      redirect_to @user, error: "Trasaction refused."
    else
      render plain: res.body.to_s
    end

    # render plain: res.body
    # render plain: User.find(2).name

    # if res.body.to_s == "true"
    #   render plain: "okay"
    # elsif res.body.to_s == "false"
    #   render plain: "no"
    # end
  end

  def answer
    params.permit!

    # @response = Response.find params[:request]

    # @response.update waiting: false, accepted: params[:response].to_s == "true"
    # @response.update waiting: false

    # render plain: User.all.to_s
    # render plain: params.inspect
    render plain: params.inspect
    # render plain: params[:response]
    # render plain: Response.find(id: params[:resquest]).id
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:brid, :name)
    end
end
