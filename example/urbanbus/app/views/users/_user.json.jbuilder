json.extract! user, :id, :brid, :name, :created_at, :updated_at
json.url user_url(user, format: :json)
