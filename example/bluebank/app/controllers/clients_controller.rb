class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, :only => [:connect, :debit]

  # GET /clients
  # GET /clients.json
  def index
    @clients = Client.all
  end

  def connect
    params.permit!

    if Client.where(brid: params[:id]).empty?
      @client = Client.new(
        brid: params[:id],
        name: params[:name],
        balance: 10
      )

      notice = 'Welcome, ' + params[:name] + '! You start with $10 in your account!'

      if @client.save
        redirect_to @client, notice: notice
      end
    else
      @client = Client.where(brid: params[:id]).first

      notice = 'Welcome back, ' + params[:name] + '!'

      redirect_to @client, notice: notice
    end
  end

  def debit
    params.permit!

    # render plain: Client.where(brid: params[:brid].to_i).first

    value = params[:data].to_f

    @client = Client.where(brid: params[:brid].to_i).first
    if @client.balance - value >= 0
      Transaction.create!(brid: params[:brid], value: -value, description: params[:name])

      @client.update(balance: @client.balance - value)

      require "net/http"
      require "json"
  
      # uri = URI(params[:callback])
      # http = Net::HTTP.new(uri.host, uri.port)
      # req = Net::HTTP::Post.new(uri.path)
      # req.set_form_data({
      #   request: params[:request],
      #   response: true
      # })
      # res = http.request(req)
      render plain: true
    else
      # uri = URI(params[:callback])
      # http = Net::HTTP.new(uri.host, uri.port)
      # req = Net::HTTP::Post.new(uri.path)
      # req.set_form_data({
      #   request: params[:request],
      #   response: false
      # })
      # res = http.request(req)
      render plain: false
    end

    # render plain: res.body
    # render plain: params.inspect
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    @transactions = Transaction.where(brid: @client.brid).order(created_at: :desc).limit(10)
  end

  # GET /clients/new
  def new
    @client = Client.new
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  # POST /clients.json
  def create
    @client = Client.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to @client, notice: 'Client was successfully created.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:brid, :name, :balance)
    end
end
