json.extract! client, :id, :brid, :name, :balance, :created_at, :updated_at
json.url client_url(client, format: :json)
