Rails.application.routes.draw do
  resources :clients

  post "clients/connect"
  post "clients/debit"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "main/index"

  root "main#index"

end
