# Banking example

Represents the interactions betwwen three SaaS:

1. Auth server **BRID** in [http://localhost:3000/](http://localhost:3000/)
1. Bank **BlueBank** in [http://localhost:3001/](http://localhost:3001/)
1. Transportation company **UrbanBus** in [http://localhost:3002/](http://localhost:3002/)

## Steps

1. In three differents terminals, run:

  ```shell
  make start_brid
  ```

  ```shell
  make start_bluebank
  ```

  ```shell
  make start_urbanbus
  ```

1. Navigate to [http://localhost:3000/natural_people](http://localhost:3000/natural_people) and create a new natural person

1. Navigate to [http://localhost:3001/](http://localhost:3001/), click "Enter with brid" and select de person you've just created. The page will be redirected

1. In anotehr tab, navigate to [http://localhost:3002/](http://localhost:3002/) and do the same. Keep this page

1. Click "Pay fare: \$3" thre times. The fourth will alert an error

1. Refresh the first tab. The transactions made in **UrbanBus**' server will be logged in this page

## Breaking integration

It is possible to stop the interaction of **UrbanBus** with **BlueBank** through **BRID** with any of those:

1. `UPDATE` **UrbanBus**'s key in **BRID**
1. `UPDATE` any field of **UrbanBus** and **BlueBank** `debit` interaction in **BRID**
1. `DELETE` **UrbanBus** and **BlueBank** `debit` interaction in **BRID**
1. `DELETE` the natural person created in **BRID**
1. `DELETE` **UrbanBus**' service in **BRID** (irriversible)
1. `DELETE` **BlueBank**'s service in **BRID** (irriversible)