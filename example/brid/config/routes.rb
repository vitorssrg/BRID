Rails.application.routes.draw do
  resources :interactions
  resources :services
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "main/index"
  get "main/export"
  post "main/connect"
  post "main/interact"

  resources :natural_people

  root "main#index"

end
