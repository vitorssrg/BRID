class MainController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:connect, :interact]

  def index
    @logs = Log.all.order(created_at: :desc).limit(10)
  end

  def export
    output = ""

    data = NaturalPerson.all.to_a.map(&:attributes)
    data.each {|x| x.delete("id")}
    data.each {|x| x["birth_date"] = x["birth_date"].to_i}
    data.each {|x| x["created_at"] = x["created_at"].to_i}
    data.each {|x| x["updated_at"] = x["updated_at"].to_i}
    output += data.to_yaml.to_s + "\n\n"

    data = Log.all.to_a.map(&:attributes)
    data.each {|x| x.delete("id")}
    data.each {|x| x["created_at"] = x["created_at"].to_i}
    data.each {|x| x["updated_at"] = x["updated_at"].to_i}
    output += data.to_yaml.to_s + "\n\n"

    render plain: output
  end

  def connect
    @params = params.permit!

    @natural_people = NaturalPerson.all
  end

  def interact
    @params = params.permit!

    @service_from = Service.find(params[:from_id])
    @service_to = Service.find(params[:to_id])
    @interaction = Interaction.where(service_id: params[:to_id].to_i, name: params[:interaction]).first

    # render plain: @interaction.inspect

    if @service_from.key.to_s != params[:key].to_s
      raise "Invalid key."
    end

    require "net/http"
    require "json"

    uri = URI(params[:interact])
    http = Net::HTTP.new(uri.host, uri.port)
    req = Net::HTTP::Post.new(uri.path)
    req.set_form_data({
      brid: params[:brid],
      name: @service_from.name,
      data: params[:data],
      request: params[:request],
      callback: params[:callback]
    })
    res = http.request(req)

    render plain: res.body

    # render plain: params.inspect
  end
end
