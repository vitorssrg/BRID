json.extract! service, :id, :name, :key, :created_at, :updated_at
json.url service_url(service, format: :json)
