json.extract! 
    natural_person, 
    :id, 
    :name, 
    :identity, 
    :birth_date, 
    :is_male, 
    :mother_id, 
    :father_id, 
    :created_at, 
    :updated_at
json.url log_method_url(natural_person, format: :json)
