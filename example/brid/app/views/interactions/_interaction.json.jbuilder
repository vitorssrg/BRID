json.extract! interaction, :id, :service_id, :name, :link, :created_at, :updated_at
json.url interaction_url(interaction, format: :json)
