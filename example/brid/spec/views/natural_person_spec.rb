require "rails_helper"

RSpec.describe NaturalPerson, type: :model do
  context "in index page" do
    it "must show all natural people" do
      awkard_name = "ababababababababa"

      person = NaturalPerson.create!(
        name: awkard_name, 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )

      visit "/natural_people"

      expect(page).to have_content(awkard_name)
    end
  end

  context "in natural person's page" do
    it "must show natural person's data" do
      awkard_name = "ababababababababa"

      person = NaturalPerson.create!(
        name: awkard_name, 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )

      visit "/natural_people/" + person.id.to_s

      expect(page).to have_content(awkard_name)
    end
  end

  context "in natural person's registration" do
    it "must show available parents" do
      awkard_name = "ababababababababa"

      person = NaturalPerson.create!(
        name: awkard_name, 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )

      visit "/natural_people/new"

      expect(page).to have_selector("#father_id", :text => awkard_name)
    end
  end
end