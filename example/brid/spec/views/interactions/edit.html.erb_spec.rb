require 'rails_helper'

RSpec.describe "interactions/edit", type: :view do
  before(:each) do
    @interaction = assign(:interaction, Interaction.create!(
      :service_id => 1,
      :name => "MyString",
      :link => "MyString"
    ))
  end

  it "renders the edit interaction form" do
    render

    assert_select "form[action=?][method=?]", interaction_path(@interaction), "post" do

      assert_select "input[name=?]", "interaction[service_id]"

      assert_select "input[name=?]", "interaction[name]"

      assert_select "input[name=?]", "interaction[link]"
    end
  end
end
