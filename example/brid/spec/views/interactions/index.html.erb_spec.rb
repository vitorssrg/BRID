require 'rails_helper'

RSpec.describe "interactions/index", type: :view do
  before(:each) do
    assign(:interactions, [
      Interaction.create!(
        :service_id => 2,
        :name => "Name",
        :link => "Link"
      ),
      Interaction.create!(
        :service_id => 2,
        :name => "Name",
        :link => "Link"
      )
    ])
  end

  it "renders a list of interactions" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Link".to_s, :count => 2
  end
end
