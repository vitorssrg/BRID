require 'rails_helper'

RSpec.describe "interactions/new", type: :view do
  before(:each) do
    assign(:interaction, Interaction.new(
      :service_id => 1,
      :name => "MyString",
      :link => "MyString"
    ))
  end

  it "renders new interaction form" do
    render

    assert_select "form[action=?][method=?]", interactions_path, "post" do

      assert_select "input[name=?]", "interaction[service_id]"

      assert_select "input[name=?]", "interaction[name]"

      assert_select "input[name=?]", "interaction[link]"
    end
  end
end
