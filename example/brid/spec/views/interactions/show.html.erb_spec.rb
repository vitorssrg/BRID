require 'rails_helper'

RSpec.describe "interactions/show", type: :view do
  before(:each) do
    @interaction = assign(:interaction, Interaction.create!(
      :service_id => 2,
      :name => "Name",
      :link => "Link"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Link/)
  end
end
