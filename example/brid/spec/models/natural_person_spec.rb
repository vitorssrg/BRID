require "rails_helper"

RSpec.describe NaturalPerson, type: :model do
  context "with natural person's id" do
    it "must be present" do
      expect(NaturalPerson.new(id: nil)).to_not be_valid
    end

    it "must be differente then -1" do
      expect(NaturalPerson.new(id: -1)).to_not be_valid
    end
  end

  context "with natural person's name" do
    it "must be present" do
      expect(NaturalPerson.new(name: nil)).to_not be_valid
    end

    it "must be at least 3 characters long" do
      expect(NaturalPerson.new(name: "ab")).to_not be_valid
    end

    it "must contain only letters and spaces" do
      expect(NaturalPerson.new(name: "ab&5$54")).to_not be_valid
    end
  end

  context "with natural person's identity" do
    it "must be present" do
      expect(NaturalPerson.new(identity: nil)).to_not be_valid
    end

    it "must be unique" do
      NaturalPerson.create!(
        name: "test", 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )

      expect(NaturalPerson.new(identity: "1234")).to_not be_valid
    end
  end

  context "with natural person's birthdate" do
    it "must be present" do
      expect(NaturalPerson.new(birth_date: nil)).to_not be_valid
    end
  end

  context "with natural person's gender" do
    it "must be present" do
      expect(NaturalPerson.new(is_male: nil)).to_not be_valid
    end
  end

  context "with natural person's mother" do
    it "must be a valid person" do
      expect {NaturalPerson.create(mother_id: -1)}.to raise_error("Mother doesn't exist.")
    end

    it "must be female" do
      mother = NaturalPerson.create!(
        name: "mother", 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )

      expect(NaturalPerson.new(mother_id: mother.id)).to_not be_valid
    end

    it "isn't possible to update a mother's gender" do
      mother = NaturalPerson.create!(
        name: "mother", 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )
      son = NaturalPerson.create!(
        name: "son", 
        identity: "2", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true,
        mother_id: mother.id
      )

      expect {mother.update(is_male: false)}.to raise_error("Can't change gender of parents.")
    end

    it "isn't possible to delete a mother" do
      mother = NaturalPerson.create!(
        name: "mother", 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: false
      )
      son = NaturalPerson.create!(
        name: "son", 
        identity: "2", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true,
        mother_id: mother.id
      )

      expect {mother.destroy}.to raise_error("Can't delete mother with children.")
    end
  end


  context "with natural person's father" do
    it "must be a valid person" do
      expect {NaturalPerson.create(father_id: -1)}.to raise_error("Father doesn't exist.")
    end

    it "must be male" do
      father = NaturalPerson.create!(
        name: "father", 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: false
      )

      expect(NaturalPerson.new(father_id: father.id)).to_not be_valid
    end

    it "isn't possible to update a father's gender" do
      father = NaturalPerson.create!(
        name: "father", 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )
      son = NaturalPerson.create!(
        name: "son", 
        identity: "2", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true,
        father_id: father.id
      )

      expect {father.update(is_male: false)}.to raise_error("Can't change gender of parents.")
    end

    it "isn't possible to delete a father" do
      father = NaturalPerson.create!(
        name: "father", 
        identity: "1", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true
      )
      son = NaturalPerson.create!(
        name: "son", 
        identity: "2", 
        birth_date: DateTime.new(1, 1, 1), 
        is_male: true,
        father_id: father.id
      )

      expect {father.destroy}.to raise_error("Can't delete father with children.")
    end
  end
end