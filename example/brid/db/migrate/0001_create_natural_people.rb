class CreateNaturalPeople < ActiveRecord::Migration[5.2]
  def change
    create_table :natural_people do |table|
      table.string :name
      table.string :identity
      table.timestamp :birth_date
      table.boolean :is_male
      table.references :mother, index: true
      table.references :father, index: true

      table.timestamps
    end

    add_foreign_key :natural_people, :natural_people, column: :mother_id
    add_foreign_key :natural_people, :natural_people, column: :father_id
  end
end
