class CreateInteractions < ActiveRecord::Migration[5.2]
  def change
    create_table :interactions do |t|
      t.integer :service_id
      t.string :name
      t.string :link

      t.timestamps
    end
  end
end
