start:
	(sleep 3 && xdg-open http://localhost:3000) &
	(cd brid && bundle exec rails s -p 3000 -b '0.0.0.0')

test:
	(cd brid && rake db:seed RAILS_ENV=test)
	(cd brid && rspec spec --format d)

reset:
	(cd brid && rake db:migrate VERSION=0)
	(cd brid && rake db:migrate)
	(cd brid && rake db:reset)
	(cd brid && rake db:seed)

routes:
	xdg-open http://localhost:3000/rails/info/routes

.PHONY: start test reset