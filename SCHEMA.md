# SCHEMA

## log_methods

| column      | type      | constraints |
|:-----------:|:---------:|:-----------:|
| method      | string    |             |
| created_at  | datetime  |             |
| updated_at  | datetime  |             |

## log_models

| column      | type      | constraints |
|:-----------:|:---------:|:-----------:|
| model       | string    |             |
| created_at  | datetime  |             |
| updated_at  | datetime  |             |

## logs

| column      | type      | constraints |
|:-----------:|:---------:|:-----------:|
| model_id    | reference | foreign key to log_models   |
| method_id   | reference | foreign key to log_methods  |
| description | string    |             |
| created_at  | datetime  |             |
| updated_at  | datetime  |             |

## natural_people

| column      | type      | constraints |
|:-----------:|:---------:|:-----------:|
| name        | string    |             |
| identity    | string    |             |
| birth_date  | datetime  |             |
| boolean     | is_male   |             |
| mother_id   | reference | foreign key to natural_people |
| father_id   | reference | foreign key to natural_people |
| created_at  | datetime  |             |
| updated_at  | datetime  |             |