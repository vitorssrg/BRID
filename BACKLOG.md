# BACKLOG

|   COMMIT  |   TODO    |   DOING   |   DONE    |
|:---------:|:---------:|:---------:|:---------:|
| 05-28 Initital Commit | Empty docker with rails<br>Simple citizen registration | Basic `README.md` and `BACKLOG.md`
| 06-08 Base rails docker | Simple citizen registration | Basic `README.md` and `BACKLOG.md` | Empty docker with rails
| 06-11 Simple citizen handling | Simple juridical person handling<br>Simple service handling | Home page | Simple citizen handling<br>Basic `README.md` and `BACKLOG.md`<br>Basic `Makefile`
| 06-15 Basic tests and logs | Tests for logging<br>Seed database<br>Citizen edit page | Tests and logging<br>Fixing `README.md` | Tests in `README.md` and in `Makefile`
| 06-18 Citizen edit page | Simple juridical person handling<br>Simple service handling | Export database as fixture<br>`SCHEMA.md` file | Tests for logging<br>Seed database<br>Citizen edit page<br>Better controllers and views<br>Less routes and migrations