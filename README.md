# BRID

A suggestion for Brazil's legal person identifier and unified sensitive information handler.

## Prerequisites

This project requires [ruby 2.5.1](https://www.ruby-lang.org/).

## Getting started

To start the application, run on a terminal:

```shell
make start
```

And the main page must open in your web browser.

## Running tests

To run the tests, run on a terminal:

```shell
make test
```

## Versioning

Please see [BACKLOG.md](BACKLOG.md) file for version history.

## Schema

## Built With

Please see [SCHEMA.md](SCHEMA.md) file for system's database schema.

* [rails](https://rubyonrails.org/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Ackowledgments

* Marco Dimas Gubitoso
