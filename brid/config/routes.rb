Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "main/index"
  get "main/export"

  resources :natural_people

  root "main#index"

end
