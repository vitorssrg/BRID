require "rails_helper"

RSpec.describe Log, type: :model do
  context "with NaturalPerson" do
    it "must log creation" do
      awkard_name = "ababababababababa"

      visit "/natural_people/new"
      fill_in "natural_person_name", with: awkard_name
      fill_in "natural_person_identity", with: awkard_name
      click_button "Save"

      visit "/"
      expect(page).to have_content("CREATE") and
      expect(page).to have_content(awkard_name)
    end

    it "must log query" do
      awkard_name = "ababababababababa"

      visit "/natural_people/new"
      fill_in "natural_person_name", with: awkard_name
      fill_in "natural_person_identity", with: awkard_name
      click_button "Save"

      visit "/natural_people"

      visit "/"
      expect(page).to have_content("LIST ALL")
    end

    it "must log update" do
      awkard_name = "ababababababababa"

      visit "/natural_people/new"
      fill_in "natural_person_name", with: awkard_name
      fill_in "natural_person_identity", with: awkard_name
      click_button "Save"

      visit "/natural_people/1/edit"
      fill_in "natural_person_name", with: awkard_name+awkard_name
      click_button "Save"

      visit "/"
      expect(page).to have_content("UPDATE") and
      expect(page).to have_content(awkard_name+awkard_name)
    end

    it "must log delete" do
      awkard_name = "ababababababababa"

      visit "/natural_people/new"
      fill_in "natural_person_name", with: awkard_name
      fill_in "natural_person_identity", with: awkard_name
      click_button "Save"

      page.driver.delete "/natural_people/1"

      visit "/"
      expect(page).to have_content("DELETE") and
      expect(page).to have_content(awkard_name)
    end
  end
end