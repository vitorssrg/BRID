class CreateLogModels < ActiveRecord::Migration[5.2]
  def change
    create_table :log_models do |table|
      table.string :model

      table.timestamps
    end
  end
end
