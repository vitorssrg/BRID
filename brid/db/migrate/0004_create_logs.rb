class CreateLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :logs do |table|
      table.references :model, index: true
      table.references :method, index: true
      table.string :description

      table.timestamps
    end

    add_foreign_key :logs, :log_models, column: :model_id
    add_foreign_key :logs, :log_methods, column: :methods_id
  end
end
