class CreateLogMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :log_methods do |table|
      table.string :method

      table.timestamps
    end
  end
end
