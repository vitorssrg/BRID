class Log < ApplicationRecord
  # belongs_to :type
  # belongs_to :model
  belongs_to :method, optional: true
  belongs_to :model, optional: true
end
