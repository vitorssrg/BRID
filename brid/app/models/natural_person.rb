class NaturalPerson < ApplicationRecord
  belongs_to :mother, optional: true
  has_many :natural_people, through: :mother
  belongs_to :father, optional: true
  has_many :natural_people, through: :father

  validates :id, exclusion: { in: %w(-1), message: "%{value} is reserved" }
  validates :name, presence: true, 
    length: { 
      within: 3..100, 
      message: "mame's length must be at least 3 and at most 100" 
    }, 
    format: { with: /\w[\w ]\w/, message: "Invalid characters present." }
  validates :identity, presence: true, uniqueness: true
  validates :birth_date, presence: true
  validates :is_male, inclusion: { in: [true, false] }

  validate :gender_change
  validate :valid_mother
  validate :valid_father

  def destroy
    if !NaturalPerson.where(mother_id: id).empty?
      raise "Can't delete mother with children."
    end

    if !NaturalPerson.where(father_id: id).empty?
      raise "Can't delete father with children."
    end

    super
  end

  private
    def gender_change
      if is_male_changed? && self.persisted?
        if (!NaturalPerson.where(mother_id: id).empty? ||
          !NaturalPerson.where(father_id: id).empty?)
          raise "Can't change gender of parents."
        end
      end
    end

    def valid_mother
      if mother_id != nil && NaturalPerson.where(id: mother_id).empty?
        raise "Mother doesn't exist."
      end
    end

    def valid_father
      if father_id != nil && NaturalPerson.where(id: father_id).empty?
        raise "Father doesn't exist."
      end
    end
end
