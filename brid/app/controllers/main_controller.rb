class MainController < ApplicationController
  def index
    @logs = Log.all.order(created_at: :desc).limit(10)
  end

  def export
    output = ""

    data = NaturalPerson.all.to_a.map(&:attributes)
    data.each {|x| x.delete("id")}
    data.each {|x| x["birth_date"] = x["birth_date"].to_i}
    data.each {|x| x["created_at"] = x["created_at"].to_i}
    data.each {|x| x["updated_at"] = x["updated_at"].to_i}
    output += data.to_yaml.to_s + "\n\n"

    data = Log.all.to_a.map(&:attributes)
    data.each {|x| x.delete("id")}
    data.each {|x| x["created_at"] = x["created_at"].to_i}
    data.each {|x| x["updated_at"] = x["updated_at"].to_i}
    output += data.to_yaml.to_s + "\n\n"

    render plain: output
  end
end
