class NaturalPeopleController < ApplicationController
  before_action :set_natural_person, only: [:show, :edit, :update, :destroy]

  # GET /natural_people
  # GET /natural_people.json
  def index
    @natural_people = NaturalPerson.all

    Log.create!(
      method_id: LogMethod.where(method: "QUERY").first.id,
      model_id: LogModel.where(model: "NaturalPerson").first.id,
      description: "LIST ALL"
    )
  end

  # GET /natural_people/1
  # GET /natural_people/1.json
  def show
    Log.create!(
      method_id: LogMethod.where(method: "INSPECT").first.id,
      model_id: LogModel.where(model: "NaturalPerson").first.id,
      description: "INSPECT " + @natural_person.inspect.to_s
    )
  end

  # GET /natural_people/new
  def new
    @natural_person = NaturalPerson.new
    @natural_people = NaturalPerson.all
  end

  # GET /natural_people/1/edit
  def edit
  end

  # POST /natural_people
  # POST /natural_people.json
  def create
    # render plain: params
    # params.permit!
    @natural_person = NaturalPerson.new(natural_person_params)

    respond_to do |format|
      if @natural_person.save
        format.html { redirect_to @natural_person, notice: "Natural Person was successfully created." }
        format.json { render :show, status: :created, location: @natural_person }

        Log.create!(
          method_id: LogMethod.where(method: "CREATE").first.id,
          model_id: LogModel.where(model: "NaturalPerson").first.id,
          description: "CREATED " + @natural_person.inspect.to_s
        )
      else
        format.html { render :new }
        format.json { render json: @natural_person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /natural_people/1
  # PATCH/PUT /natural_people/1.json
  def update
    respond_to do |format|
      if @natural_person.update(natural_person_params)
        format.html { redirect_to @natural_person, notice: "Natural Person was successfully updated." }
        format.json { render :show, status: :ok, location: @natural_person }

        Log.create!(
          method_id: LogMethod.where(method: "UPDATE").first.id,
          model_id: LogModel.where(model: "NaturalPerson").first.id,
          description: "UPDATED " + @natural_person.inspect.to_s
        )
      else
        format.html { render :edit }
        format.json { render json: @natural_person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /natural_people/1
  # DELETE /natural_people/1.json
  def destroy
    @natural_person.destroy

    Log.create!(
      method_id: LogMethod.where(method: "DELETE").first.id,
      model_id: LogModel.where(model: "NaturalPerson").first.id,
      description: "DELETED " + @natural_person.inspect.to_s
    )

    respond_to do |format|
      format.html { redirect_to natural_people_url, notice: "Natural Person was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_natural_person
      @natural_person = NaturalPerson.find(params[:id])
      @natural_people = NaturalPerson.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def natural_person_params
      params.require(:natural_person).permit(
        :name, 
        :identity, 
        "birth_date(1i)",
        "birth_date(2i)",
        "birth_date(3i)",
        :gender
      )
      params.require(:mother_id)
      params.require(:father_id)

      name = params[:natural_person][:name]
      identity = params[:natural_person][:identity]
      birth_date = DateTime.new(
        params[:natural_person]["birth_date(1i)"].to_i,
        params[:natural_person]["birth_date(2i)"].to_i,
        params[:natural_person]["birth_date(3i)"].to_i
      )
      is_male = params[:natural_person][:gender] == "male"
      mother_id = params[:mother_id].to_i
      father_id = params[:father_id].to_i

      permitted = {
        name: name, 
        identity: identity, 
        birth_date: birth_date,
        is_male: is_male
      }

      if mother_id != -1
        permitted[:mother_id] = mother_id
      end
  
      if father_id != -1
        permitted[:father_id] = father_id
      end

      permitted
    end
end
